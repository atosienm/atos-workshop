# Closing notes


# Reference material

- Slides are available as Docker image on Docker Hub, [Slides](https://hub.docker.com/r/atosjava/jfall2015slides/)
- All sources used for the handson lab are available on BitBucket, [GIT](https://bitbucket.org/jcz-jfall/atos-workshop)
- DIY: Setup a (VM) machine running Docker 1.7.1 and Docker Compose 1.4.2


![stroopwafel](images/keep-calm-and-eat-a-stroopwafel.png)
